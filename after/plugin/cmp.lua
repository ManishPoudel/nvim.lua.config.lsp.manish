-- Autocompletion --

local cmp = require('cmp')
local lspkind=require('lspkind')
cmp.config.enabled=true
cmp.setup({
  snippet = {
    expand = function(args)
      -- You might need to install `luasnip` for snippets support
      require('luasnip').lsp_expand(args.body)
    end,
  },
    window = {
         --completion = cmp.config.window.bordered(),
        documentation ={
            --cmp.config.window.bordered(),
            max_width=100,
            --auto_open=true,
        }
        -- Try to set the width of the documentation
            --auto_open = true,
            --max_width=200,
    },
    view = {
        docs={
        auto_open=true,
        },
    },

    formatting = {
    format = lspkind.cmp_format({
            -- for text and symbol do symbol_text
      mode = 'symbol', -- show only symbol annotations
      maxwidth = 60, -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)
                     -- can also be a function to dynamically calculate max width such as 
                     -- maxwidth = function() return math.floor(0.45 * vim.o.columns) end,
      ellipsis_char = '...', -- when popup menu exceed maxwidth, the truncated part would show ellipsis_char instead (must define maxwidth first)
    preset = 'codicons',


      -- The function below will be called before any actual modifications from lspkind
      -- so that you can provide more controls on popup customization. (See [#30](https://github.com/onsails/lspkind-nvim/pull/30))
      before = function (entry, vim_item)
        return vim_item
      end
    })
  },

  mapping = cmp.mapping.preset.insert({
        -- When showing documentation it scrolls the doc for reading.
    ['<C-b>'] = cmp.mapping.scroll_docs(-2),
    ['<C-f>'] = cmp.mapping.scroll_docs(2),
        -- scroll the Autocompletion text
    ["<C-j>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
    ["<C-k>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),

    -- Tab also works for auto completion
    ['<Tab>'] = cmp.mapping.confirm({ select = true }),
    -- For Enter to auto completion
    ['<CR>'] = cmp.mapping.confirm({ select = true }),
        -- Show the documentation of the cursor text.
  }),
  sources =cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
    { name = 'buffer' },
    {name = 'path'},
  })
})

--    require('lspkind').init({
--        -- DEPRECATED (use mode instead): enables text annotations
--        --
--        -- default: true
--        -- with_text = true,
--    
--        -- defines how annotations are shown
--        -- default: symbol
--        -- options: 'text', 'text_symbol', 'symbol_text', 'symbol'
--        mode = 'symbol_text',
--    
--        -- default symbol map
--        -- can be either 'default' (requires nerd-fonts font) or
--        -- 'codicons' for codicon preset (requires vscode-codicons font)
--        --
--        -- default: 'default'
--        preset = 'codicons',
--    
--        -- override preset symbols
--        --
--        -- default: {}
--        symbol_map = {
--          Text = "󰉿",
--          Method = "󰆧",
--          Function = "󰊕",
--          Constructor = "",
--          Field = "󰜢",
--          Variable = "󰀫",
--          Class = "󰠱",
--          Interface = "",
--          Module = "",
--          Property = "󰜢",
--          Unit = "󰑭",
--          Value = "󰎠",
--          Enum = "",
--          Keyword = "󰌋",
--          Snippet = "",
--          Color = "󰏘",
--          File = "󰈙",
--          Reference = "󰈇",
--          Folder = "󰉋",
--          EnumMember = "",
--          Constant = "󰏿",
--          Struct = "󰙅",
--          Event = "",
--          Operator = "󰆕",
--          TypeParameter = "",
--        },
--    })

----Auto Completion--
--local cmp_action = lsp_zero.cmp_action()
--cmp.setup({
--  mapping = cmp.mapping.preset.insert({
--    -- `Enter` key to confirm completion
--    ['<CR>'] = cmp.mapping.confirm({select = false}),
--
--    -- Ctrl+Space to trigger completion menu
--    ['<C-Space>'] = cmp.mapping.complete(),
--
--    -- Navigate between snippet placeholder
--    ['<C-f>'] = cmp_action.luasnip_jump_forward(),
--    ['<C-b>'] = cmp_action.luasnip_jump_backward(),
--
--    -- Scroll up and down in the completion documentation
--    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
--    ['<C-d>'] = cmp.mapping.scroll_docs(4),
--  }),
--     sources = cmp.config.sources({
--        { name = 'nvim_lsp' },
--        { name = 'luasnip', option = { show_autosnippets = true } },
--        { name = 'buffer' },
--        { name = 'async_path' },
--        { name = 'neorg' },
--    })
--
--}
--)

