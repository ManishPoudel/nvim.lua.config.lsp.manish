local plugins={
    {
    'nvim-telescope/telescope.nvim', tag = '0.1.5',
-- or                              , branch = '0.1.x',
      dependencies = { 'nvim-lua/plenary.nvim' }
    },

    { 'rose-pine/neovim', name = 'rose-pine' },

{
"nvim-treesitter/nvim-treesitter",
build = ":TSUpdate",
config = function ()
  local configs = require("nvim-treesitter.configs")

  configs.setup({
      ensure_installed = { "c","cpp", "lua", "vim", "vimdoc", "query", "javascript", "html" },
      sync_install = false,
      highlight = { enable = true },
      indent = { enable = true },
    })
end
    },
  -- Autocompletion
  {
    'hrsh7th/nvim-cmp',
        run = ":CmpStatus",
    dependencies = {
      {'L3MON4D3/LuaSnip'}
    },
    },

      {
    'neovim/nvim-lspconfig',
    dependencies = {
      {'hrsh7th/cmp-nvim-lsp'},
    }
  },

    {
    'VonHeikemen/lsp-zero.nvim',
    branch = 'v3.x',
    lazy = true,
  },
    {'hrsh7th/cmp-buffer'},
    --lsp-zero
 -- {'VonHeikemen/lsp-zero.nvim', branch = 'v3.x'},
 -- {'neovim/nvim-lspconfig'},
 -- {'hrsh7th/cmp-nvim-lsp'},
 -- {'hrsh7th/nvim-cmp'},
 -- {'L3MON4D3/LuaSnip'},
    --lsp-zero over
    --
    --Completion Menu costumization.
    {'onsails/lspkind.nvim'},
    -- for proper display of errors 
    {'nvim-lua/lsp-status.nvim'},
}

local opts={}

require("lazy").setup(plugins,opts)
