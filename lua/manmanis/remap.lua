--- Keymapping ---
vim.g.mapleader=" "
--set in normal mode, if space followed by pv then execute vim command Ex
vim.keymap.set("n","<leader>nr",vim.cmd.Ex)

--vim.keymap.set("i","C-h","C-w")

--- Key-Bindings ---
-- Map Shift+Enter in insert mode to insert a new line and move the cursor
--vim.api.nvim_set_keymap('i', '<S-CR>', '<Esc>o', { noremap = true, silent = true })
-- vim.keymap.set("i","<S-CR>",'<Esc>o')

-- Delete a word with Ctrl+Backspace in insert mode
--vim.api.nvim_set_keymap('i', '<C-BS>', '<C-w>', { noremap = true, silent = true })


-- Map Shift+Enter in insert mode to insert a new line and move the cursor
--vim.api.nvim_set_keymap('i', '<S-CR>', [=[<C-\><C-n>:normal! o<CR>]=], { noremap = true, silent = true })


