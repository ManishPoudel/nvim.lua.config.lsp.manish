local lsp_zero = require('lsp-zero')

vim.o.updatetime=200
lsp_zero.on_attach(function(client, bufnr)
  -- see :help lsp-zero-keybindings
  -- to learn the available actions
    -- for diagnostic window.
    vim.api.nvim_create_autocmd("CursorHold", {
  buffer = bufnr,
  callback = function()
    local opts = {
      focusable = false,
      close_events = { "BufLeave", "CursorMoved", "InsertEnter", "FocusLost" },
      border = 'rounded',
      source = 'always',
      prefix = ' ',
      scope = 'cursor',
    }
    vim.diagnostic.open_float(nil, opts)
  end
})

  lsp_zero.default_keymaps({buffer = bufnr})
end)
--With this too it ran
--require('lspconfig').clangd.setup({})

local lua_opts = lsp_zero.nvim_lua_ls()
require('lspconfig').lua_ls.setup(lua_opts)
--Added later
local capabilities = require('cmp_nvim_lsp').default_capabilities()
  -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
  require('lspconfig')['clangd'].setup {
    capabilities = capabilities
  }


-- lsp status line
--local lsp_status = require('lsp-status')
--local lspconfig = require('lspconfig')
--
---- Some arbitrary servers
--lspconfig.clangd.setup({
--  handlers = lsp_status.extensions.clangd.setup(),
--  init_options = {
--    clangdFileStatus = true
--  },
--  on_attach = lsp_status.on_attach,
--  capabilities = lsp_status.capabilities
--})
