require("manmanis.remap")
require("manmanis.lazy")
require("manmanis.plugins")
--require("manmanis.cmpnvimlsp")
--require("manmanis.cmp")
require("manmanis.lsp")
require("manmanis.set")
-- For testing purposes
--print("hello From manmanish/init.lua")
